package repozytorium;

import java.sql.ResultSet;
import java.sql.SQLException;
import User.Entity;

public interface CreateEntity<TEntity extends Entity> {

	public TEntity build(ResultSet rs) throws SQLException;
	
}