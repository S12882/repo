package repozytorium;

import java.util.*;
import User.*;

public interface RepoCatalog {
	
	public UserRepo getUsers();
	public Repository<Person> getPersons();
	public Repository<UserRoles> getRoles();
	public void commit();
}


