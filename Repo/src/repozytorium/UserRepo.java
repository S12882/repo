package repozytorium;

import java.util.*;
import User.*;

public interface UserRepo extends Repository<User> {

	public User withName(String name);
	public User withIntKey(int key ,String name); 
	public User withStringKey(String key, String name);
	
}
