package repozytorium;

import java.util.*;
import java.sql.*;

import unitOfWork.UnitOfWork;
import User.*;

public class UserRepository implements UserRepo {
	
	
	private Connection connection;

	private String url = "jdbc:hsqldb:hsql://localhost/workdb";

	private String createTablePerson = "CREATE TABLE Person(VARCHAR(50) name, ...)";

	private PreparedStatement addPersonStmt;
	private PreparedStatement deleteAllPersonsStmt;
	private PreparedStatement getAllPersonsStmt;
	private PreparedStatement CountStmt;
	private PreparedStatement UpdateStmt;
	private PreparedStatement DELETEStmt;

	private Statement statement;
	
	public UserRepository(Connection connection, CreateUser createUser, UnitOfWork unit) {
		super();
		
		try {
			connection = DriverManager.getConnection(url);
			statement = connection.createStatement();

			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean tableExists = false;
			while (rs.next()) {
				if ("Person".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableExists = true;
					break;
				}
			}

			if (!tableExists)
				statement.executeUpdate(createTablePerson);

			addPersonStmt = connection
					.prepareStatement("INSERT INTO Person (name, surname, pesel ) VALUES (?, ?, ?)");
			deleteAllPersonsStmt = connection
					.prepareStatement("DELETE * FROM Person");
			getAllPersonsStmt = connection
					.prepareStatement("SELECT * FROM Person");
			CountStmt = connection
					.prepareStatement("SELECT COUNT() FROM Person;");
			UpdateStmt = connection
					.prepareStatement("UPDATE Person");
			DELETEStmt = connection
					.prepareStatement("DELETE FROM Person WHERE 'pesel' like ");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	

	public void add(User entity) {
		try {
			addPersonStmt.setString(1, Person.getFirstName());
			addPersonStmt.setString(2, Person.getSurname());
			addPersonStmt.setString(3, Person.getPesel());

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void modify(User entity) {
	
	}

	@Override
	public void delete(User entity) {
		try {
			DELETEStmt.setString(1, Person.getPesel());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void count() {
		try {
			CountStmt.setString(1 , "*");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public User withName(String name) {
	
		return null;
	}

	@Override
	public User withIntKey(int key, String name) {
		
		return null;
	}

	@Override
	public User withStringKey(String key, String name) {
		
		return null;
	}

}
