package rules;


import java.util.Date ;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import User.*;

public class DateOfBirthRule implements CheckRule<CheckResult> {
	final static String DATE_FORMAT = "dd-MM-yyyy";
	
	public void CheckResult(CheckResult entity) {
	
		Date today = new Date();
		Date input = Person.getDate();
       if(input.after(today)){
		 entity.setResult(RuleResult.Error);
       }
       else{entity.setResult(RuleResult.Ok);}
		
	}

}
