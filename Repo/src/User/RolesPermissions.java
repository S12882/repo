package User;

import java.util.*;

public class RolesPermissions extends Entity {
 private int roleId;
 private int permissionId;
 private List<UserRoles> roles;
 
 public RolesPermissions(){
	 
	 roles = new ArrayList<UserRoles>();
 }
 
 public int getRoleID(){
	 return roleId;
 }
 
 public void SetRoleId(int roleId){
	 this.roleId = roleId;
 }
 
 public int getPermissionId(){
	 return permissionId;
 }
 
 public void SetPermisionId(int permissionId){
	 this.permissionId = permissionId;
 }
 
 public List<UserRoles> getRoles(){
	 return roles;
 }
 
 public void setRoles (List<UserRoles> roles){
	 this.roles = roles;
 }
}
