package User;

import java.util.ArrayList;
import java.util.List;

public class User extends Entity {
	private String login;
	private String password;
	private Person person;
	
	private List<UserRoles> roles;
	
	public User()
	{
		roles = new ArrayList<UserRoles>();
	}
	
	public String getLogin(){
		return login;
	}
	
	public String getPass(){
		return password;
	}
	
	public void setLogin(String login){
		this.login = login;
	}
	
	public void setPass(String password){
		this.password = password;
	}
	
	public List<UserRoles>getRoles(){
		return roles;
	}
	
	public Person getPerson(){
		return person;
	}
	
	public void setPerson(Person person){
		this.person = person;
		
	}
}
