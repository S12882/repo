package User;

public class Addres extends Entity {

	private String countryId;
	private String regionId;
	private String city;
	private String zipCode;
	private String street;
	private String houseNumber;
	private String localNumber;
	
	private Person person;
	
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public String getCountry() {
		return countryId;
	}
	public void setCountry(String country) {
		this.countryId = country;
	}
	
	public String getRegion() {
		return regionId;
	}
	public void setRegion(String country) {
		this.regionId = country;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostalCode() {
		return zipCode;
	}
	public void setPostalCode(String postalCode) {
		this.zipCode = postalCode;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getLocalNumber() {
		return localNumber;
	}
	public void setLocalNumber(String localNumber) {
		this.localNumber = localNumber;
	}
	
	
}